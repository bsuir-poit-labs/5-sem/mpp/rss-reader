﻿using System;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace RSS
{
    public class RssParser
    {
        private const string RssTag = "rss";
        private const string ChannelTag = "channel";
        private const string TitleTag = "title";
        private const string ItemTag = "item";
        private const string DescriptionTag = "description";
        private const string PubDateTag = "pubDate";
        private const string LinkTag = "link";
        
        private readonly Regex _regex = new Regex(@"(<.*\/>)(.*)(<.*\/>)");

        private readonly XDocument _document;

        public string Title
        {
            get
            {
                var title = _document.Element(RssTag)?.Element(ChannelTag)?.Element(TitleTag);
                return title?.Value; 
            }
        }

        public RssParser(string uri)
        {
            _document = XDocument.Load(uri);
        }

        public void ShowNews()
        {
            var items = _document.Element(RssTag)?.
                Element(ChannelTag)?.Elements(ItemTag);

            if (items == null)
            {
                Console.WriteLine("Не удалось загрузить элементы!");
                return;
            }
            
            foreach (var elem in items)
            {
                var title = elem.Element(TitleTag);
                var description = elem.Element(DescriptionTag);
                var pubDate = elem.Element(PubDateTag);
                var link = elem.Element(LinkTag);

                if (description != null)
                {
                    var match = _regex.Match(description.Value);
                    description.Value = match.Groups[2].Value;
                }

                var dateTime = Convert.ToDateTime(pubDate?.Value);

                Console.WriteLine($"    {title?.Value.ToUpper()}");
                Console.WriteLine($"{description?.Value}");
                Console.WriteLine($"{dateTime:d MMM yyyy} {link?.Value}");
                Console.WriteLine();
            }
        }
    }
}