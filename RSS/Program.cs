﻿using System;
using System.Linq;

namespace RSS
{
    class Program
    {
        private static readonly string[] Urls = {
            "https://news.tut.by/rss/health.rss",
            "https://news.tut.by/rss/auto.rss",
            "https://news.tut.by/rss/sport.rss"
        };

        static void Main(string[] args)
        {
            var parsers = Urls.Select(url => new RssParser(url)).ToList();

            int action;
            do
            {
                for (int i = 0; i < parsers.Count; i++)
                {
                    Console.WriteLine("{0}. {1}",  i + 1, parsers[i].Title);
                }
                Console.WriteLine("0. Выход");
                
                Console.Write("Выберите рубрику: ");
                action = Convert.ToInt32(Console.ReadLine());

                if (action != 0)
                {
                    parsers[action - 1].ShowNews();
                }
                
            } while (action != 0);
            
        }
    }
}