﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using Multithreaded_RSS_Mail_Sender.Mail;

namespace Multithreaded_RSS_Mail_Sender.Rss
{
    public static class RssFeed
    {
        private static readonly Regex Regex = new(@"(<.*\/>)(.*)(<.*\/>)");

        public static void PerformFeed(string url, string[] keyWords, string[] emails)
        {
            var waitHandler = new ManualResetEvent(false);
            var feed = LoadFeed(url);

            ThreadPool.QueueUserWorkItem(_ =>
            {
                feed = FilterFeed(feed, keyWords);
                waitHandler.Set();
            });
            waitHandler.WaitOne();

            var strFeed = RssFeedToString(feed);

            ThreadPool.QueueUserWorkItem(_ => SendFeed(emails, strFeed));

            ThreadPool.QueueUserWorkItem(_ => Console.WriteLine(strFeed));
        }

        private static SyndicationFeed LoadFeed(string url)
        {
            return SyndicationFeed.Load(XmlReader.Create(url));
        }

        private static SyndicationFeed FilterFeed(SyndicationFeed feed, string[] keyWords)
        {
            if (keyWords.Length == 0)
            {
                return feed;
            }
            
            var syndicationItems = feed.Items.ToArray();
            var filterFeedItems = new List<SyndicationItem>();

            foreach (var keyWord in keyWords)
            {
                foreach (var item in syndicationItems)
                {
                    if (item.Summary.Text.ToUpper().Contains(keyWord.ToUpper()) && !filterFeedItems.Contains(item))
                    {
                        filterFeedItems.Add(item);
                    }
                }
            }

            feed.Items = filterFeedItems;
            return feed;
        }

        private static void SendFeed(string[] emails, string strFeed)
        {
            foreach (var email in emails)
            {
                MailService.SendEmail(email, strFeed);
            }
        }

        private static string RssFeedToString(SyndicationFeed feed)
        {
            var stringBuilder = new StringBuilder();
            var feedItems = feed.Items.ToArray();

            foreach (var item in feedItems)
            {
                var match = Regex.Match(item.Summary.Text);
                var summary = match.Groups[2].Value;

                stringBuilder.Append($"\t{item.Title.Text.ToUpper()}\n")
                    .Append($"{summary}\n")
                    .Append($"{item.PublishDate:d MMM yyy}г. ")
                    .Append($"{item.Links[0].Uri}\n\n");
            }

            return stringBuilder.ToString();
        }
    }
}